package com.lookren.transitad.server;

import com.lookren.transitad.shared.PlaceItem;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public enum Dao {
    INSTANCE;

    @SuppressWarnings("unchecked")
    public List<PlaceItem> listPlaceItems() {
        EntityManager em = EMFService.getInstance().get().createEntityManager();
        Query q = em.createQuery(new String("select m from PlaceItem m ORDER BY m.timeAdded"));
        return q.getResultList();
    }

    public void addPlaceItem(double latitude, double longitude, String address) {
        synchronized (this) {
            EntityManager em = EMFService.getInstance().get().createEntityManager();
            PlaceItem placeItem = new PlaceItem(latitude, longitude, address);
            em.persist(placeItem);
            em.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<PlaceItem> getPlaceItems(String address, double latitude, double longitude) {
        EntityManager em = EMFService.getInstance().get().createEntityManager();
        Query q = em.createQuery(new String("select t from PlaceItem t " +
                "where t.address = :address and t.latitude = :latitude and t.longitude = :longitude"));
        q.setParameter("address", address);
        q.setParameter("latitude", latitude);
        q.setParameter("longitude", longitude);
        return q.getResultList();
    }

    public void removePlaceItem(long id) {
        EntityManager em = EMFService.getInstance().get().createEntityManager();
        try {
            PlaceItem todo = em.find(PlaceItem.class, id);
            em.remove(todo);
        } finally {
            em.close();
        }
    }
}

