package com.lookren.transitad.server;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
public class EMFService {
    private EntityManagerFactory emfInstance;

    private static EMFService emf;

    private EMFService() {
    }

    public EntityManagerFactory get() {
        if(emfInstance == null) {
            emfInstance = Persistence.createEntityManagerFactory("transactions-optional");
        }
        return emfInstance;
    }

    public static EMFService getInstance() {
        if(emf == null) {
            emf = new EMFService();
        }
        return emf;
    }
}
