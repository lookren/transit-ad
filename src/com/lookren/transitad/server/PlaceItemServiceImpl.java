package com.lookren.transitad.server;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.lookren.transitad.client.PlaceItemService;
import com.lookren.transitad.shared.FieldVerifier;
import com.lookren.transitad.shared.PlaceItem;
import com.lookren.transitad.shared.RouteItem;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

@SuppressWarnings("serial")
public class PlaceItemServiceImpl extends RemoteServiceServlet implements PlaceItemService {

    public static final Logger log = Logger.getLogger(PlaceItemService.class.getName());

    public void addPlaceItem(String address, double latitude, double longitude) throws IllegalArgumentException {
        if (!FieldVerifier.isValidName(address) || !FieldVerifier.isValidCoordinate(latitude)
                || !FieldVerifier.isValidCoordinate(longitude)) {
            throw new IllegalArgumentException(
                    "Input is invalid");
        }
        address = escapeHtml(address);
        Dao dao = Dao.INSTANCE;
        List<PlaceItem> exist = dao.getPlaceItems(address, latitude, longitude);
        if (exist == null || exist.isEmpty()) {
            dao.addPlaceItem(latitude, longitude, address);
        } else {
            log.info("*** add place: already exist");
        }
    }

    @Override
    public void removePlaceItem(long id) throws IllegalArgumentException {
        Dao.INSTANCE.removePlaceItem(id);
    }

    @Override
    public List<PlaceItem> listPlaceItems() {
        final List<PlaceItem> returnList = new ArrayList<PlaceItem>();
        for (PlaceItem each : Dao.INSTANCE.listPlaceItems()) {
            returnList.add(each);
        }
        return returnList;
    }

    @Override
    public List<RouteItem> listRoutes(int radius, int amount) {
        List<RouteItem> result = getSortedByRateRouteItems(radius, amount);
        log.info("*** list routes result:" + result.size() + ": " + result);
        return result;
    }

    private List<RouteItem> getSortedByRateRouteItems(int radius, int amount) {
        Map<RouteItem, Integer> allRoutesWithRate = new HashMap<RouteItem, Integer>();
        for (PlaceItem eachPlace : Dao.INSTANCE.listPlaceItems()) {
            List<RouteItem> routeItems = getRoutesFromApiForPlaceItem(eachPlace, radius);
            if (routeItems != null) {
                for (RouteItem eachRoute : routeItems) {
                    if (eachRoute == null) {
                        continue;
                    }
                    Integer currentRate = allRoutesWithRate.get(eachRoute);
                    if (currentRate == null) {
                        currentRate = 0;
                    }
                    log.info("*** for route " + eachRoute.getId() + " rate is: " + currentRate);
                    allRoutesWithRate.put(eachRoute, currentRate + 1);
                }
            }
        }
        List<RouteItem> arrayByRate[] = new ArrayList[allRoutesWithRate.size()];
        for (Map.Entry<RouteItem, Integer> each : allRoutesWithRate.entrySet()) {
            if (arrayByRate[each.getValue()] == null) {
                arrayByRate[each.getValue()] = new ArrayList<RouteItem>();
            }
            arrayByRate[each.getValue()].add(each.getKey());
        }

        List<RouteItem> result = new ArrayList<RouteItem>();
        for (int i = arrayByRate.length - 1; i >= 0; i--) {
            List<RouteItem> routeItemList = arrayByRate[i];
            if (routeItemList == null) {
                continue;
            }
            for (RouteItem eachRoute : routeItemList) {
                if (result.size() < amount) {
                    result.add(eachRoute);
                }
            }
        }
        return result;
    }

    private List<RouteItem> getRoutesFromApiForPlaceItem(PlaceItem placeItem, int radius) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(QueryString.PARAM_LATITUDE, String.valueOf(placeItem.getLatitude()));
        params.put(QueryString.PARAM_LONGITUDE, String.valueOf(placeItem.getLongitude()));
        params.put(QueryString.PARAM_RADIUS, String.valueOf(radius));
        List<RouteItem> routeItems = null;
        try {
            URL url = new URL(new QueryString(params).toString());
            log.info("*** request for " + placeItem.getAddress() + ", API params:" + url);

            JsonReader reader = new JsonReader(new InputStreamReader(url.openStream()));
            reader.setLenient(true);
            try {
                Gson gson = new Gson();
                RouteItem[] routeItemsArray = gson.fromJson(reader, RouteItem[].class);
                log.info("*** getRoutesFromApiForPlaceItemArray: "
                        + (routeItemsArray != null ? routeItemsArray.length : null));
                if (routeItemsArray != null) {
                    routeItems = Arrays.asList(routeItemsArray);
                }
                reader.close();
            } catch (JsonSyntaxException e) {
                log.info("*** JsonSyntaxException " + e + "occurred for " + url);
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            log.info(e.getMessage());
        } catch (IOException e) {
            log.info(e.getMessage());
        }
        return routeItems;
    }

    private String escapeHtml(String html) {
        if (html == null) {
            return null;
        }
        return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }
}
