package com.lookren.transitad.server;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class QueryString {

    private static final String WIKIROUTES_API_URL = "http://wikiroutes.info/userService/radiusSearchTemporary?";
    private String query = WIKIROUTES_API_URL;
    
    public static final String PARAM_LATITUDE = "lat";
    public static final String PARAM_LONGITUDE = "lon";
    public static final String PARAM_RADIUS= "radius";

    public QueryString() {
        query = WIKIROUTES_API_URL;
    }

    public QueryString(Map<String, String> map) {
        this();
        boolean ampNeeded = false;
        for (Map.Entry<String, String> each : map.entrySet()) {
            if (ampNeeded) {
                query += "&";
            }
            String str = getParametersString(each.getKey(), each.getValue());
            if (str != null && !str.isEmpty()) {
                query += str;
                if (!ampNeeded) {
                    ampNeeded = true;
                }
            }
        }
    }

    public QueryString(Object name, Object value) {
        this();
        query = getParametersString(name, value);
    }

    public synchronized void add(Object name, Object value) {
        if (!query.trim().equals(WIKIROUTES_API_URL)) {
            query += "&";
        }
        query += getParametersString(name, value);

    }

    private static String getParametersString(Object name, Object value) {
        try {
            return URLEncoder.encode(name.toString(), "utf-8") + "=" +
                    URLEncoder.encode(value.toString(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String toString() {
        return query;
    }
}
