package com.lookren.transitad.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.lookren.transitad.shared.PlaceItem;
import com.lookren.transitad.shared.RouteItem;

import java.util.List;

public interface PlaceItemServiceAsync {

    void addPlaceItem(String address, double latitude, double longitude, AsyncCallback<Void> async);

    void removePlaceItem(long id, AsyncCallback<Void> async);

    void listPlaceItems(AsyncCallback<List<PlaceItem>> async);

    void listRoutes(int radius, int amount, AsyncCallback<List<RouteItem>> async);

}
