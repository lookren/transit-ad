package com.lookren.transitad.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.lookren.transitad.shared.PlaceItem;
import com.lookren.transitad.shared.RouteItem;

import java.util.List;

@RemoteServiceRelativePath("place")
public interface PlaceItemService extends RemoteService {
	void addPlaceItem(String address, double latitude, double longitude) throws IllegalArgumentException;
	void removePlaceItem(long id) throws IllegalArgumentException;
	List<PlaceItem> listPlaceItems();
	List<RouteItem> listRoutes(int radius, int amount);
}
