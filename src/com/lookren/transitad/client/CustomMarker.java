package com.lookren.transitad.client;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.base.Point;
import com.google.gwt.maps.client.overlays.overlayhandlers.OverlayViewMethods;
import com.google.gwt.maps.client.overlays.overlayhandlers.OverlayViewOnAddHandler;
import com.google.gwt.maps.client.overlays.overlayhandlers.OverlayViewOnDrawHandler;
import com.google.gwt.maps.client.overlays.overlayhandlers.OverlayViewOnRemoveHandler;

public class CustomMarker implements OverlayViewOnAddHandler, OverlayViewOnDrawHandler, OverlayViewOnRemoveHandler {

    private static final int WIDTH = 16;
    private static final int PADDING = 4;
    private static final int BORDER_WIDTH = 1;
    private static final int FONT_SIZE = 12;
    private static final String BORDER_COLOUR = "#999999";
    private static final String BACKGROUND_COLOUR = "#FFFFFF";
    private Element label;
    private LatLng latLng;
    private String text;

    public CustomMarker() {
        this(null, null);
    }

    public CustomMarker(String text, LatLng latLng) {
        this.latLng = latLng;
        this.text = text;
        Document document = Document.get();
        label = document.createLabelElement();
    }

    @Override
    public void onDraw(OverlayViewMethods methods) {
        Point position = methods.getProjection().fromLatLngToDivPixel(latLng);
        Style style = label.getStyle();
        style.setDisplay(Display.BLOCK);
        style.setBackgroundColor(BACKGROUND_COLOUR);
        style.setBorderColor(BORDER_COLOUR);
        style.setBorderStyle(BorderStyle.SOLID);
        style.setBorderWidth(BORDER_WIDTH, Unit.PX);
        style.setPadding(PADDING, Unit.PX);
//        style.setPaddingTop(7, Unit.PX);
        style.setMarginTop(7, Unit.PX);

        style.setPosition(Position.ABSOLUTE);
        style.setLeft(position.getX() - (WIDTH / 2 + PADDING), Unit.PX);
        style.setTop(position.getY() - WIDTH, Unit.PX);
        style.setWidth(WIDTH, Unit.PX);
        style.setHeight(WIDTH, Unit.PX);
        style.setFontSize(FONT_SIZE, Unit.PX);
        style.setTextAlign(Style.TextAlign.CENTER);
        style.setDisplay(Display.BLOCK);
        style.setBorderStyle(BorderStyle.SOLID);
        style.setBorderColor(BORDER_COLOUR);
        style.setColor(BORDER_COLOUR);
        style.setPropertyPx("border-radius", 22);
        style.setProperty("text-shadow", "0 1px 0 #eee");
        style.setTextAlign(Style.TextAlign.CENTER);
        style.setTextDecoration(Style.TextDecoration.NONE);

        label.setInnerHTML(text);
    }

    @Override
    public void onAdd(OverlayViewMethods methods) {
        methods.getPanes().getOverlayLayer().appendChild(label);
    }

    @Override
    public void onRemove(OverlayViewMethods methods) {
        label.getParentNode().removeChild(label);
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPosition(LatLng latLng) {
        this.latLng = latLng;
    }
}
