package com.lookren.transitad.client;

import com.google.gwt.ajaxloader.client.ArrayHelper;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.maps.client.LoadApi;
import com.google.gwt.maps.client.LoadApi.LoadLibrary;
import com.google.gwt.maps.client.MapOptions;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.controls.ControlPosition;
import com.google.gwt.maps.client.events.click.ClickMapEvent;
import com.google.gwt.maps.client.events.click.ClickMapHandler;
import com.google.gwt.maps.client.events.domready.DomReadyMapEvent;
import com.google.gwt.maps.client.events.domready.DomReadyMapHandler;
import com.google.gwt.maps.client.events.rightclick.RightClickMapEvent;
import com.google.gwt.maps.client.events.rightclick.RightClickMapHandler;
import com.google.gwt.maps.client.overlays.*;
import com.google.gwt.maps.client.services.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.lookren.transitad.shared.FieldVerifier;
import com.lookren.transitad.shared.PlaceItem;
import com.lookren.transitad.shared.RouteItem;

import java.util.ArrayList;
import java.util.List;

public class TransitAd implements EntryPoint {

    private static final String[] COLORS = new String[]{
            "#00ffff", "#f00fff", "#0000ff", "#0a0aca", "#1cc432",
            "#f86433", "#99ccff", "#b2b6b6", "#c0000f", "#99970f",
            "#ecc0ee", "#bbaa22", "#f0ffff", "#a3d210", "#eee888",
            "#d5a5d8", "#994499", "#555553", "#aa440f", "#ff0000"
    };

    private static final String[] ROUTE_TYPES = new String[]{"", "автобус",
            "маршрутка", "метро", "троллейбус", "трамвай", "монорельс", "водный транспорт"};

    private static final String SERVER_ERROR = "An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.";

    private final PlaceItemServiceAsync placeService = GWT.create(PlaceItemService.class);

    private static final int MAP_ZOOM_DEFAULT = 10;
    private MapWidget map;
    private TextBox searchField;
    private Label radiusField;
    private Label amountField;
    private DialogBox errorDialogBox;
    private DialogBox waitDialogBox;
    private Button closeButton;
    private Label textToServerLabel;
    private HTML serverResponseLabel;
    private VerticalPanel list;
    final private List<Polyline> polylinesAdded = new ArrayList<Polyline>();
    final private List<OverlayView> markersAdded = new ArrayList<OverlayView>();
    private VerticalPanel mapLegendWidget;
    private InfoWindow infoWindow;

    public void onModuleLoad() {
        boolean sensor = true;

        ArrayList<LoadLibrary> loadLibraries = new ArrayList<LoadApi.LoadLibrary>();
        loadLibraries.add(LoadLibrary.ADSENSE);
        loadLibraries.add(LoadLibrary.DRAWING);
        loadLibraries.add(LoadLibrary.GEOMETRY);
        loadLibraries.add(LoadLibrary.PANORAMIO);
        loadLibraries.add(LoadLibrary.PLACES);
        loadLibraries.add(LoadLibrary.WEATHER);
        loadLibraries.add(LoadLibrary.VISUALIZATION);

        Runnable onLoad = new Runnable() {
            @Override
            public void run() {
                buildUi();
            }
        };

        LoadApi.go(onLoad, loadLibraries, sensor);
    }

    private void buildUi() {
        MapOptions mapOptions = MapOptions.newInstance();
        mapOptions.setScaleControl(true);
        mapOptions.setZoomControl(true);

        mapOptions.setMapTypeControl(true);
        mapOptions.setScaleControl(true);
        mapOptions.setScrollWheel(true);
        map = new MapWidget(MapOptions.newInstance());
        map.setSize("100%", "100%");

        searchField = TextBox.wrap(DOM.getElementById("pac-input"));
        searchField.setFocus(true);
        searchField.selectAll();
        searchField.setTitle("Найти адрес");

        SearchPlaceHandler handler = new SearchPlaceHandler();
        searchField.addKeyUpHandler(handler);
        map.addRightClickHandler(new RightClickMapHandler() {
            @Override
            public void onEvent(RightClickMapEvent event) {
                performReverseLookup(event.getMouseEvent().getLatLng(), false);
            }
        });
        map.addClickHandler(new ClickMapHandler() {
            @Override
            public void onEvent(ClickMapEvent event) {
                performReverseLookup(event.getMouseEvent().getLatLng(), false);
            }
        });

        final Geolocation geolocation = Geolocation.getIfSupported();
        if (geolocation != null) {
            geolocation.getCurrentPosition(handler);
        }

        RootPanel.get("mapContainer").add(map);

        initErrorDialogBox();
        initWaitDialogBox();

        list = new VerticalPanel();
        RootPanel.get("listContainer").add(list);

        radiusField = Label.wrap(DOM.getElementById("area"));
        amountField = Label.wrap(DOM.getElementById("amount"));
        Button calculateButton = Button.wrap(DOM.getElementById("calculate"));
        calculateButton.addClickHandler(new CalculateClickHandler());

        listPlacesFromServer();
    }

    private void listPlacesFromServer() {
        showWaitDialog();
        placeService.listPlaceItems(new AsyncCallback<List<PlaceItem>>() {
            @Override
            public void onFailure(Throwable caught) {
                hideWaitDialog();
                showErrorDialog("can't get list of places from server");
            }

            @Override
            public void onSuccess(List<PlaceItem> result) {
                if (mapLegendWidget != null) {
                    mapLegendWidget.setVisible(false);
                    mapLegendWidget.removeFromParent();
                }
                hideWaitDialog();
                list.clear();
                for (OverlayView eachMarker : markersAdded) {
                    eachMarker.setMap(null);
                }
                for (Polyline each : polylinesAdded) {
                    each.setMap(null);
                }
                polylinesAdded.clear();
                markersAdded.clear();
                if (infoWindow != null) {
                    infoWindow.close();
                    infoWindow = null;
                }
                for (int i = 0; i < result.size(); i++) {
                    PlaceItem placeItem = result.get(i);
                    OptionalTextBox listItem = new OptionalTextBox(placeItem, (i + 1));
                    list.add(listItem);

                    CustomMarker customMarkerHandler = new CustomMarker("" + (i + 1),
                            LatLng.newInstance(placeItem.getLatitude(), placeItem.getLongitude()));
                    OverlayView marker = OverlayView.newInstance(map, customMarkerHandler, customMarkerHandler, customMarkerHandler);
                    marker.setMap(map);
                    markersAdded.add(marker);
                }
            }
        });
    }

    private void removePlaceFromServer(String id) {
        if (FieldVerifier.isValidId(id)) {
            showWaitDialog();
            placeService.removePlaceItem(Long.valueOf(id), new AsyncCallback<Void>() {
                @Override
                public void onFailure(Throwable caught) {
                    hideWaitDialog();
                    showErrorDialog("can't remove place from server");
                }

                @Override
                public void onSuccess(Void result) {
                    listPlacesFromServer();
                }
            });
        }
    }

    private void addPlaceToServer(String address, double latitude, double longitude) {
        showWaitDialog();
        textToServerLabel.setText(address);
        serverResponseLabel.setText("");
        searchField.setText("");
        placeService.addPlaceItem(address, latitude, longitude,
                new AsyncCallback<Void>() {
                    public void onFailure(Throwable caught) {
                        hideWaitDialog();
                        showErrorDialog("can't add place to server");
                    }

                    public void onSuccess(Void result) {
                        listPlacesFromServer();
                    }
                });
    }

    private void listRoutesFromServer(int radius, int amount) {
        placeService.listRoutes(radius, amount, new AsyncCallback<List<RouteItem>>() {
            @Override
            public void onFailure(Throwable caught) {
                hideWaitDialog();
                showErrorDialog("calculate");
            }

            @Override
            public void onSuccess(List<RouteItem> result) {
                hideWaitDialog();

                displayRoutesResult(result);
            }
        });
    }

    private void displayRoutesResult(List<RouteItem> result) {
        if (mapLegendWidget != null) {
            mapLegendWidget.setVisible(false);
            mapLegendWidget.removeFromParent();
        }
        for (Polyline each : polylinesAdded) {
            each.setMap(null);
        }
        polylinesAdded.clear();
        for (OverlayView each : markersAdded) {
            each.setMap(null);
        }
        for (int routeIndex = 0; routeIndex < result.size(); routeIndex++) {
            RouteItem each = result.get(routeIndex);
            int sizeArray = each.getCoordinates().length;
            int pointsArrayLength = 0;
            for (int coordinateslistIndex = 0; coordinateslistIndex < sizeArray; coordinateslistIndex++) {
                pointsArrayLength += each.getCoordinates()[coordinateslistIndex].size();
            }
            LatLng[] points = new LatLng[pointsArrayLength];
            int pointsArrayIndex = 0;
            for (int listIndex = 0; listIndex < sizeArray; listIndex++) {
                ArrayList<String> coordinates = each.getCoordinates()[listIndex];
                for (String coordinatePairString : coordinates) {
                    try {
                        final Double latitude = Double.valueOf(coordinatePairString.split(" ")[0]);
                        final Double longitude = Double.valueOf(coordinatePairString.split(" ")[1]);
                        LatLng point = LatLng.newInstance(latitude, longitude);
                        points[pointsArrayIndex] = point;
                        pointsArrayIndex++;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }

            PolylineOptions polylineOptions = PolylineOptions.newInstance();
            JsArray<LatLng> latLngJsArray = ArrayHelper.toJsArray(points);
            polylineOptions.setPath(latLngJsArray);
            polylineOptions.setStrokeColor(getColorByIndex(routeIndex));
            polylineOptions.setStrokeOpacity(1.0);
            polylineOptions.setStrokeWeight(2);

            final Polyline polyline = Polyline.newInstance(polylineOptions);
            polylinesAdded.add(polyline);
            polyline.setMap(map);
        }

        mapLegendWidget = new VerticalPanel();
        mapLegendWidget.setStyleName("map-legend");
        for (int i = 0; i < result.size(); i++) {
            RouteItem each = result.get(i);
            String color = i >= COLORS.length ? COLORS[i % COLORS.length] : COLORS[i];
            HorizontalPanel horizontalPanel = new HorizontalPanel();
            Label type = new Label();
            Label name = new Label();
            type.setText(ROUTE_TYPES[each.getTypeId()] + " ");
            name.setText(each.getName());
            HTML label = new HTML(
                    "<span style=\"background-color: " + color + "; height: 5px; width: 30px; margin: 3px; " +
                            "border: 1px solid #000; color: " + color + "\">.</span>");

            horizontalPanel.add(label);
            horizontalPanel.add(type);
            horizontalPanel.add(name);
            mapLegendWidget.add(horizontalPanel);
        }
        map.setControls(ControlPosition.BOTTOM_LEFT, mapLegendWidget);

        for (OverlayView each : markersAdded) {
            each.setMap(map);
        }
    }

    private void initErrorDialogBox() {
        errorDialogBox = new DialogBox();
        errorDialogBox.setText("Remote Procedure Call");
        errorDialogBox.setAnimationEnabled(true);
        closeButton = new Button("Close");
        closeButton.getElement().setId("closeButton");
        textToServerLabel = new Label();
        serverResponseLabel = new HTML();
        VerticalPanel dialogVPanel = new VerticalPanel();
        dialogVPanel.addStyleName("dialogVPanel");
        dialogVPanel.add(new HTML("<b>RPC to the server:</b>"));
        dialogVPanel.add(textToServerLabel);
        dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
        dialogVPanel.add(serverResponseLabel);
        dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
        dialogVPanel.add(closeButton);
        errorDialogBox.setWidget(dialogVPanel);
        closeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                errorDialogBox.hide();
                searchField.setFocus(true);
            }
        });
    }

    private void initWaitDialogBox() {
        waitDialogBox = new DialogBox();
        waitDialogBox.setSize("150px", "150px");
        waitDialogBox.setAnimationEnabled(true);
        Image image = new Image();
        image.setUrl("http://www.wsbonline.com/Images/Icons/Processing.gif");
        waitDialogBox.setWidget(image);
    }

    private void showErrorDialog(String error) {
        errorDialogBox.setText(error + ": Remote Procedure Call - Failure");
        serverResponseLabel
                .addStyleName("serverResponseLabelError");
        serverResponseLabel.setHTML(SERVER_ERROR);
        errorDialogBox.center();
        closeButton.setFocus(true);
    }

    private void showWaitDialog() {
        waitDialogBox.center();
        waitDialogBox.show();
    }

    private void hideWaitDialog() {
        waitDialogBox.hide();
        searchField.setFocus(true);
    }

    private void performLookup(final String address, boolean addPlaceItem) {
        if (address != null && !address.isEmpty() && map != null) {
            final Geocoder geocoder = Geocoder.newInstance();
            GeocoderRequest geocoderRequest = GeocoderRequest.newInstance();
            geocoderRequest.setAddress(address);
            geocoder.geocode(geocoderRequest, new PlaceLocationRequestHandler(true, addPlaceItem));
        }
    }

    private void performReverseLookup(final LatLng point, boolean addPlaceItem) {
        if (point != null && map != null) {
            final Geocoder geocoder = Geocoder.newInstance();
            GeocoderRequest geocoderRequest = GeocoderRequest.newInstance();
            geocoderRequest.setLocation(point);
            geocoder.geocode(geocoderRequest, new PlaceLocationRequestHandler(false, addPlaceItem));
        }
    }

    class SearchPlaceHandler implements KeyUpHandler, Callback<Position, PositionError> {

        public void onKeyUp(KeyUpEvent event) {
            if (FieldVerifier.isValidName(searchField.getText())) {
                performLookup(searchField.getText(), event.getNativeKeyCode() == KeyCodes.KEY_ENTER);
            }
        }

        @Override
        public void onFailure(PositionError positionError) {
        }

        @Override
        public void onSuccess(Position position) {
            Position.Coordinates coordinates = position.getCoordinates();
            map.setCenter(LatLng.newInstance(coordinates.getLatitude(), coordinates.getLongitude()));
            map.setZoom(MAP_ZOOM_DEFAULT);
        }
    }

    private class CalculateClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            showWaitDialog();
            final String radiusString = radiusField.getText();
            final String amountString = amountField.getText();
            int radius = 200;
            int amount = 10;
            try {
                radius = Integer.valueOf(radiusString);
                amount = Integer.valueOf(amountString);
            } catch (NumberFormatException e) {
            }
            listRoutesFromServer(radius, amount);
        }
    }

    private class PlaceLocationRequestHandler implements GeocoderRequestHandler {
        boolean highlightOnMap;
        boolean addPlaceItem;

        PlaceLocationRequestHandler(boolean highlightOnMap, boolean addPlaceItem) {
            this.highlightOnMap = highlightOnMap;
            this.addPlaceItem = addPlaceItem;
        }
        @Override
        public void onCallback(JsArray<GeocoderResult> results, GeocoderStatus status) {
            if (results.length() > 0) {
                final LatLng point = results.get(0).getGeometry().getLocation();
                final String address = results.get(0).getFormatted_Address();
                  makeInfo(point, address);
                if (!highlightOnMap) {
                    searchField.setText(address);
                }
                if (addPlaceItem) {
                    addPlaceToServer(address, point.getLatitude(), point.getLongitude());
                }
            }
        }
    }

    private static int nextAnchorId = 1;

    public InfoWindow makeInfo(final LatLng point, final String address) {
        InfoWindowOptions infoWindowOptions = InfoWindowOptions.newInstance();
        infoWindowOptions.setPosition(point);
        FlowPanel infoContentWidget = new FlowPanel();
        final String theAnchorIdStr = "theAnchor" + nextAnchorId;
//        FocusPanel img = new FocusPanel();
//        img.setStyleName("map-point");
        final HTML theAnchor = new HTML("<div class=\"title\" id=\"" + theAnchorIdStr + "\">Поставить точку</div>");
//        infoContentWidget.add(img);
        infoContentWidget.add(theAnchor);
        infoWindowOptions.setContent(infoContentWidget.getElement());
        final InfoWindow infoWindow = InfoWindow.newInstance(infoWindowOptions);
        infoWindow.open(map);
        infoWindow.addDomReadyHandler(new DomReadyMapHandler() {

            @Override
            public void onEvent(DomReadyMapEvent event) {
                com.google.gwt.user.client.Element muffinButton = (com.google.gwt.user.client.Element) Document.get().getElementById(theAnchorIdStr);
                DOM.sinkEvents(muffinButton, Event.ONCLICK);
                DOM.setEventListener(muffinButton, new EventListener() {
                    @Override
                    public void onBrowserEvent(Event event) {
                        addPlaceToServer(address, point.getLatitude(), point.getLongitude());
                        infoWindow.close();
                    }
                });
            }
        });

        nextAnchorId++;
        return infoWindow;
    }

    private String getColorByIndex(int index) {
        return (index >= COLORS.length ? COLORS[index % COLORS.length] : COLORS[index]);
    }

    private String getMarkerColorByIndex(int index) {
        return COLORS[COLORS.length - 1];
    }

    class OptionalTextBox extends Composite implements ClickHandler {
        private Label textBox = new Label();
        private Label indexBox = new Label();
        private FocusPanel remove = new FocusPanel();
        private TextBox id = new TextBox();
        private int index;

        public OptionalTextBox(PlaceItem placeItem, int index) {
            this.index = index;
            DockPanel panel = new DockPanel();
            panel.add(indexBox, DockPanel.WEST);
            panel.add(remove, DockPanel.EAST);
            panel.add(textBox, DockPanel.CENTER);
            panel.setHorizontalAlignment(DockPanel.ALIGN_LEFT);

            setPlaceItem(placeItem);
            remove.addClickHandler(this);

            initWidget(panel);
            indexBox.setStyleName("title");
            textBox.setStyleName("title");
            textBox.setWidth("300px");
            remove.setStyleName("remove");
            id.setVisible(false);
            setStyleName("search-list__item");
        }

        private void setPlaceItem(PlaceItem placeItem) {
            indexBox.setText(String.valueOf(index) + ". ");
            textBox.setText(placeItem.getAddress());
            id.setText(String.valueOf(placeItem.getId()));
        }

        public void onClick(ClickEvent event) {
            Object sender = event.getSource();
            if (sender == remove) {
                removePlaceFromServer(id.getValue());
            }
        }
    }
}
