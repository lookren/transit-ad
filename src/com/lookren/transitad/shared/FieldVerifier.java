package com.lookren.transitad.shared;

public class FieldVerifier {
    private final static int COORDINATE_MIN_VALUE = -80;
    private final static int COORDINATE_MAX_VALUE = 80;

    public static boolean isValidName(String name) {
        if (name == null) {
            return false;
        }
        return name.length() > 3;
    }

    public static boolean isValidCoordinate(Double value) {
        if (value == null || value > COORDINATE_MAX_VALUE || value < COORDINATE_MIN_VALUE) {
            return false;
        }
        return true;
    }

    public static boolean isValidId(String id) {
        if (id == null || id.isEmpty()) {
            return false;
        }
        try {
            Long.valueOf(id);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
