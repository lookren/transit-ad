package com.lookren.transitad.shared;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class PlaceItem implements Serializable {
	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double latitude;
    private double longitude;
    private String address;
    private long timeAdded;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public PlaceItem(double latitude, double longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.timeAdded = System.currentTimeMillis();
    }

    public PlaceItem() {
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddress() {
        return address;
    }

    public String name() {
        return address;
    }

    public long getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(long timeAdded) {
        this.timeAdded = timeAdded;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (o.getClass() != PlaceItem.class) {
            return false;
        }
        PlaceItem placeItem = (PlaceItem) o;
        if (latitude == placeItem.latitude) {
            if (longitude == placeItem.longitude) {
                if (address != null) {
                    return address.equals(placeItem.address);
                } else {
                    return placeItem.address == null;
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) (latitude + longitude + (address != null ? address.hashCode() : 0));
    }

    @Override
    public String toString() {
        return latitude + ", " + longitude;
    }
}
