package com.lookren.transitad.shared;

import java.io.Serializable;
import java.util.ArrayList;

public class RouteItem implements Serializable {
	private static final long serialVersionUID = 1L;

    private Long id;
    private Long placeItemId;
    private Integer typeId;
    private String name;
    private ArrayList<String> coordinates[];

    public RouteItem() {
    }

    public RouteItem(long id, int typeId, String name, ArrayList<String>[] coordinates) {
        this.id = id;
        this.typeId = typeId;
        this.name = name;
        this.coordinates = coordinates;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlaceItemId() {
        return placeItemId;
    }

    public void setPlaceItemId(Long id) {
        this.placeItemId = id;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String>[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<String>[] coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "RouteItem{" +
                "id=" + id +
                ", typeId=" + typeId +
                ", name='" + name + '\'' +
                ", coordinates ='" + (coordinates != null ? coordinates[0] : null) + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RouteItem routeItem = (RouteItem) o;
        if (id == null) {
            return routeItem.id == null;
        } else if (routeItem.id == null) {
            return false;
        }
        if (!id.equals(routeItem.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
