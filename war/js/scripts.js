$(document).ready(function(){
	if (document.getElementById('ui-slider')) {
		$('#ui-slider').slider({
			min: 100,
			max: 1000,
			step: 100,
			value: 500,
			create: function( event, ui ) {
				$('#area').text($('#ui-slider').slider('value'));
			},
			slide: function( event, ui ) {
				$('#area').text(ui.value);
			}
		});
	}
	if (document.getElementById('ui-slider-1')) {
		$('#ui-slider-1').slider({
			min: 1,
			max: 20,
			step: 1,
			value: 10,
			create: function( event, ui ) {
				$('#amount').text($('#ui-slider-1').slider('value'));
			},
			slide: function( event, ui ) {
				$('#amount').text(ui.value);
			}
		});
	}

	if (document.getElementById('fullpage')) {
		$('#fullpage').fullpage({
			anchors: ['firstPage', 'secondPage', 'thirdPage'],
			verticalCentered: false,
			scrollBar: true,
			resize : true,
			afterLoad: function(anchorLink, index){
				$('.page-navigation__item').removeClass('current');
	            $('.page-navigation__item').eq(index - 1).addClass('current');
	        }
		});
	}

	$('.page-navigation__item').on('click', function() {
		$(this).addClass('current');
		$(this).siblings().removeClass('current');
		$.fn.fullpage.moveTo($(this).index() + 1);
		return false;
	});

	$('.search-input').on('keyup', function() {
		if ($(this).val().length) $(this).parent().addClass('add');
		else $(this).parent().removeClass('add');
	});
});
